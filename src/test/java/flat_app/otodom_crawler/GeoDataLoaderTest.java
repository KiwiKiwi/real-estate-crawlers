// package flat_app.otodom_crawler;
//
// import flat_app.geo_data.GeoDataLoader;
// import org.junit.Test;
// import flat_app.geo_data.GeoData;
//
// import static org.junit.Assert.assertEquals;
//
// public class GeoDataLoaderTest {
//
//    GeoDataLoader geoDataLoader = new GeoDataLoader();
//
//    @Test
//    public void parseJsonResponseTest1() {
//        String raw =
// "{\"Response\":{\"MetaInfo\":{\"Timestamp\":\"2018-10-16T05:17:42.289+0000\"},\"View\":[{\"_type\":\"SearchResultsViewType\",\"ViewId\":0,\"Result\":[{\"Relevance\":1.0,\"MatchLevel\":\"city\",\"MatchQuality\":{\"City\":1.0},\"Location\":{\"LocationId\":\"NT_iyRTQPgMUnhCqJ6Z45p-KB\",\"LocationType\":\"point\",\"DisplayPosition\":{\"Latitude\":53.11931,\"Longitude\":18.0081},\"NavigationPosition\":[{\"Latitude\":53.11931,\"Longitude\":18.0081}],\"MapView\":{\"TopLeft\":{\"Latitude\":53.20943,\"Longitude\":17.87388},\"BottomRight\":{\"Latitude\":53.05032,\"Longitude\":18.19858}},\"Address\":{\"Label\":\"Bydgoszcz, Woj. Kujawsko-Pomorskie, Polska\",\"Country\":\"POL\",\"State\":\"Woj. Kujawsko-Pomorskie\",\"County\":\"Bydgoszcz\",\"City\":\"Bydgoszcz\",\"PostalCode\":\"85-023\",\"AdditionalData\":[{\"value\":\"Polska\",\"key\":\"CountryName\"},{\"value\":\"Woj. Kujawsko-Pomorskie\",\"key\":\"StateName\"},{\"value\":\"Bydgoszcz\",\"key\":\"CountyName\"}]}}}]}]}}";
//        GeoData geoCoordinates = geoDataLoader.parseResponse(raw).get();
//        assertEquals("53.11931", geoCoordinates.getLatitude());
//        assertEquals("18.0081", geoCoordinates.getLongitude());
//    }
// }
