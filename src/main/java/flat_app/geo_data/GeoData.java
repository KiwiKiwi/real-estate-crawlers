package flat_app.geo_data;

import javax.persistence.*;
import lombok.*;

@NoArgsConstructor
@ToString
@Entity
@Getter
public class GeoData {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Integer id;

  @Column String latitude;
  @Column String longitude;
  @Column String label;
  @Column String country;
  @Column String state;
  @Column String county;
  @Column String city;
  @Column String district;
  @Column String subDistrict;
  @Column String street;
  @Column String postalCode;

  GeoData(
          String latitude,
          String longitude,
          String label,
          String country,
          String state,
          String county,
          String city,
          String district,
          String subDistrict,
          String street,
          String postalCode) {
    this.latitude = latitude;
    this.longitude = longitude;
    this.label = label;
    this.country = country;
    this.state = state;
    this.county = county;
    this.city = city;
    this.district = district;
    this.subDistrict = subDistrict;
    this.street = street;
    this.postalCode = postalCode;
  }
}
