package flat_app.geo_data;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import flat_app.flat.Flat;
import flat_app.flat.FlatDao;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
@NoArgsConstructor
public class GeoDataLoader {

    @Autowired FlatDao dao;

    @Value("${heremaps.applicationId}")
    private String applicationId ;
    @Value("${heremaps.applicationCode}")
    private String applicationCode;
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final String heremapsUrl =
            "http://geocoder.api.here.com/6.2/geocode.json?app_id=" + applicationId
                    + "&app_code=" + applicationCode;

    public void fillFlatsWithCoordinates() {
        List<Flat> flats = dao.selectFlatsWithoutCoordinates();

        flats.forEach(
                flat -> {
                    String address = flat.getAddress().getCity() + " " + flat.getAddress().getRest();
                    Optional<GeoData> coordinates = getCoordinates(address);
                    if (coordinates.isPresent()) {
                        flat.setGeoData(coordinates.get());
                        dao.save(flat);
                    } else {
                        logger.info("Getting only city part for flat_app.flat " + flat.toString());
                        Optional<GeoData> city = getCoordinates(flat.getAddress().getCity());
                        if (city.isPresent()) {
                            flat.setGeoData(city.get());
                            dao.save(flat);
                        } else
                            logger.warn(
                                    "Empty coordinates for flat_app.flat " + flat.link + ", address " + flat.getAddress());
                    }
                });
    }

    private Optional<GeoData> getCoordinates(String address) {
        String addr = null;
        addr = heremapsUrl + URLEncoder.encode(address.replaceAll(" {2}", " "), StandardCharsets.UTF_8);

        String json = null;
        try {
            json =
                    Jsoup.connect(addr)
                            .ignoreContentType(true)
                            .method(Connection.Method.GET)
                            .execute()
                            .body();
        } catch (IOException e) {
            logger.error("Couldn't get coordinates for address: " + address, e);
            return Optional.empty();
        }
        return parseResponse(json);
    }

    public Optional<GeoData> parseResponse(String json) {
        Gson gson = new Gson();
        if (json == null) {
            logger.error("Geolocation data is null");
            return Optional.empty();
        }

        JsonObject jsonObject = gson.fromJson(json, JsonObject.class);
        if (jsonObject.getAsJsonObject("Response").getAsJsonArray("View").size() == 0)
            return Optional.empty();

        try {
            Optional<JsonElement> max =
                    StreamSupport.stream(
                            List.of(
                                    jsonObject
                                            .getAsJsonObject("Response")
                                            .getAsJsonArray("View")
                                            .getAsJsonArray()
                                            .get(0)
                                            .getAsJsonObject()
                                            .getAsJsonArray("Result")
                                            .getAsJsonArray())
                                    .get(0)
                                    .getAsJsonArray()
                                    .spliterator(),
                            false)
                            .max(
                                    Comparator.comparingDouble(
                                            e -> Double.valueOf(e.getAsJsonObject().get("Relevance").getAsString())));

            if (!max.isPresent()) return Optional.empty();

            JsonElement addressPart =
                    max.get().getAsJsonObject().get("Location").getAsJsonObject().get("Address");
            String label = getValue(addressPart.getAsJsonObject(), "Label");
            String country = getValue(addressPart.getAsJsonObject(), "Country");
            String state = getValue(addressPart.getAsJsonObject(), "State");
            String county = getValue(addressPart.getAsJsonObject(), "County");
            String city = getValue(addressPart.getAsJsonObject(), "City");
            String district = getValue(addressPart.getAsJsonObject(), "District");
            String postalCode = getValue(addressPart.getAsJsonObject(), "PostalCode");

            String subdistrict = getValue(addressPart.getAsJsonObject(), "Subdistrict");
            String street = getValue(addressPart.getAsJsonObject(), "Street");

            JsonElement position =
                    max.get().getAsJsonObject().get("Location").getAsJsonObject().get("DisplayPosition");
            String latitude = position.getAsJsonObject().get("Latitude").getAsString();
            String longitude = position.getAsJsonObject().get("Longitude").getAsString();

            return Optional.of(
                    new GeoData(
                            latitude,
                            longitude,
                            label,
                            country,
                            state,
                            county,
                            city,
                            district,
                            subdistrict,
                            street,
                            postalCode));
        } catch (Exception e) {
            logger.error("Error when parsing GeoData response for " + json);
            return Optional.empty();
        }
    }

    private String getValue(JsonObject o, String s) {
        if (o == null) return null;
        else {
            if (o.get(s) != null) return o.get(s).getAsString();
            else return null;
        }
    }
}
