package flat_app.otodom_crawler;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

@Service
class PageDownloader {

  private final Logger logger = LogManager.getLogger(this.getClass());
  private Map<String, String> cookies = new HashMap<>();
  private String referrer = "";

  Optional<Document> getPage(String url) {
    try {
      return getPage(url, 5);
    } catch (InterruptedException e) {
      return Optional.empty();
    }
  }

  private Optional<Document> getPage(String url, int retryCount) throws InterruptedException {
    try {
      Thread.sleep(500);
      String userAgent =
          "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36";
      Connection.Response response =
          Jsoup.connect(url)
              .referrer(referrer)
              .cookies(cookies)
              .header("upgrade-insecure-requests", "1")
              .userAgent(userAgent)
              .method(Connection.Method.GET)
              .execute();
      cookies = response.cookies();
      referrer = url;
      if (response.statusCode() < 400) return Optional.of(response.parse());
      else throw new IOException("Invalid code");
    } catch (IOException e) {
      if (retryCount == 0) return Optional.empty();
      logger.error("Connection reset for url " + url + ", retry count: " + retryCount);
      logger.error(Arrays.toString(e.getStackTrace()));
      cookies.clear();
      referrer = "";
      return getPage(url, retryCount - 1);
    }
  }
}
