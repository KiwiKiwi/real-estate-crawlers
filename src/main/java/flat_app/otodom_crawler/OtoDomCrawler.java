package flat_app.otodom_crawler;

import com.google.common.base.Preconditions;
import flat_app.flat.Address;
import flat_app.flat.Flat;
import flat_app.flat.FlatDao;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OtoDomCrawler {

  private final Logger logger = LogManager.getLogger(this.getClass());
  private final String otoDomHomeUrl = "https://www.otodom.pl/wynajem/mieszkanie/";

  @Autowired
  private PageDownloader downloader;
  @Autowired
  private FlatDao dao;

  public Integer downloadFlats() {
    AtomicInteger count = new AtomicInteger();

    Set<String> links = getLinksToFlats(Optional.empty());
    logger.info("Got " + links.size() + " unique links.");

    links
        .stream()
        .map(this::getAd)
        .flatMap(Optional::stream)
        .peek(e -> count.incrementAndGet())
        .forEach(dao::save);
    return count.get();
  }

  private Set<String> getLinksToFlats(Optional<Integer> howManyPages) {

    Set<String> links = new HashSet<>();

    Optional<Document> doc = downloader.getPage(otoDomHomeUrl);
    if (!doc.isPresent()) return new HashSet<>();
    String current = doc.get().getElementsByClass("current").text();
    Preconditions.checkState(!current.isEmpty(), "Problems with downloading main page.");

    Integer totalNumOfPages = Integer.valueOf(current);

    Stream.iterate(1, c -> ++c)
        .limit(howManyPages.orElse(totalNumOfPages))
        .forEach(c -> links.addAll(getLinksFromPage(c)));
    return links;
  }

  private Set<String> getLinksFromPage(Integer i) {
    logger.info("Getting links from page number: " + i);
    Optional<Document> page = downloader.getPage(otoDomHomeUrl + "?page=" + i);
    if (page.isPresent()) {
      Set<String> urlToFullAd = getUrlToFullAd(page.get());
      logger.info("Got " + urlToFullAd.size() + " links for page " + i);
      return urlToFullAd;
    } else {
      logger.info("Got 0 links for page " + i);
      return new HashSet<>();
    }
  }

  private Set<String> getUrlToFullAd(Document site) {
    return site.getElementsByClass("offer-item-header")
        .stream()
        .flatMap(elem -> elem.select("a[href]").stream().map(l -> l.attr("abs:href")))
        .collect(Collectors.toSet());
  }

  private Optional<Flat> getAd(String url) {
    logger.info("Getting ad with url: " + url);
    Optional<Document> page = downloader.getPage(url);
    if (!page.isPresent()) return Optional.empty();
    Document doc = page.get();
    try {

      Elements obligatoryParams = doc.getElementsByClass("main-list");
      Elements li = obligatoryParams.select("li");
      Elements leftFooter = doc.getElementsByClass("left");

      String offerNumber = leftFooter.select("p").get(0).text();
      Element price = li.get(0);
      Element area = li.get(1);
      String rooms = li.get(2).select("strong").text().trim();
      String floor = li.get(3).select("strong").text().trim();
      String address = doc.getElementsByClass("address-text").get(0).text();

      Flat flat =
          new Flat(
              parseOfferNumber(offerNumber),
              parsePrice(price),
              parseArea(area),
              Float.parseFloat(rooms),
              floor,
              url,
              parseAddress(address));
      return Optional.of(flat);
    } catch (Exception e) {
      logger.error("Parsing error when downloading url " + url);
      logger.error(e.getMessage());
      /*Log whole body on DEBUG*/
    }
    return Optional.empty();
  }

  private Double parsePrice(Element price) {
    return Double.parseDouble(
        price
            .select("strong")
            .text()
            .trim()
            .split(" zł")[0]
            .replaceAll(" ", "")
            .replaceAll(",", "."));
  }

  private Double parseArea(Element area) {
    return Double.parseDouble(
        area.select("strong")
            .text()
            .trim()
            .split(" m")[0]
            .replaceAll(" ", "")
            .replaceAll(",", "."));
  }

  private Address parseAddress(String s) {
    String[] split = s.split(",");
    String rest = Arrays.stream(split).skip(1).map(String::trim).collect(Collectors.joining(", "));
    return new Address(split[0].trim(), rest);
  }

  private Integer parseOfferNumber(String s) {
    return Integer.parseInt(s.split(":")[1].trim());
  }
}
