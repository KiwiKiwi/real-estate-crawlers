package flat_app.run_info;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RunInfoDao extends CrudRepository<RunInfo, Integer> {}
