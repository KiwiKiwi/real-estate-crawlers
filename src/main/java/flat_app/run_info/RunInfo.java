package flat_app.run_info;

import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(
    name = "otodomRunInfo",
    uniqueConstraints = {@UniqueConstraint(columnNames = "id")})
@NoArgsConstructor
public class RunInfo {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", updatable = false, nullable = false)
  Integer id;

  @Column private Date date;

  @Column Integer offerCount;

  public RunInfo(Date date, Integer offerCount) {
    this.date = date;
    this.offerCount = offerCount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    RunInfo runInfo = (RunInfo) o;
    return Objects.equals(id, runInfo.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "RunInfo{" + "id=" + id + ", date=" + date + ", offerCount=" + offerCount + '}';
  }
}
