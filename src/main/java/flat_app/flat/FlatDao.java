package flat_app.flat;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlatDao extends CrudRepository<Flat, Integer> {

  @Query("from Flat where geoData is null")
  List<Flat> selectFlatsWithoutCoordinates();

  default void add(Flat flat) {
    Optional<Flat> f = this.findById(flat.offerNumber);
    f.ifPresent(flat1 -> flat.setGeoData(flat1.geoData));
    this.save(f.get());
  }
}
