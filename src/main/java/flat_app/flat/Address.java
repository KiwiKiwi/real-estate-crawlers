package flat_app.flat;

import lombok.*;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Address {

  String city;
  String rest;

  @Override
  public String toString() {
    return "Address{" + "city='" + city + '\'' + ", rest='" + rest + '\'' + '}';
  }
}
