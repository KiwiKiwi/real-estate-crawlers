package flat_app.flat;

import flat_app.geo_data.GeoData;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

@Setter
@Entity
@Table(
    name = "otodom",
    uniqueConstraints = {@UniqueConstraint(columnNames = "offerNumber")})
@DynamicUpdate
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Flat implements Serializable {

  @Id
  @Column(name = "offerNumber")
  Integer offerNumber;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "geoDataId")
  GeoData geoData;

  @Column Double price;
  @Column Double area;
  @Column Float rooms;
  @Column String floor;
  @Column public String link;

  @Column
  @Embedded
  @AttributeOverrides({@AttributeOverride(name = "rest", column = @Column(name = "address"))})
  Address address;

  @Column @UpdateTimestamp Date lastSeenDate;

  @Column(updatable = false)
  @CreationTimestamp
  Date creationDate;

  public Flat(
      Integer offerNumber,
      Double price,
      Double area,
      Float rooms,
      String floor,
      String link,
      Address address) {
    this.offerNumber = offerNumber;
    this.price = price;
    this.area = area;
    this.rooms = rooms;
    this.floor = floor;
    this.link = link;
    this.address = address;
  }

  @Override
  public String toString() {
    return "Flat{"
        + "offerNumber="
        + offerNumber
        + ", price="
        + price
        + ", area="
        + area
        + ", rooms="
        + rooms
        + ", floor="
        + floor
        + ", link='"
        + link
        + '\''
        + ", address="
        + address
        + ", lastSeenDate="
        + lastSeenDate
        + ", creationDate="
        + creationDate
        + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Flat flat = (Flat) o;
    return Objects.equals(offerNumber, flat.offerNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(offerNumber);
  }
}
