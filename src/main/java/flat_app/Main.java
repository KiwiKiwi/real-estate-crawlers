package flat_app;

import flat_app.geo_data.GeoDataLoader;
import flat_app.otodom_crawler.OtoDomCrawler;
import flat_app.otodom_crawler.RunMode;
import flat_app.run_info.RunInfo;
import flat_app.run_info.RunInfoDao;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Main implements CommandLineRunner {

  private static final Logger logger = LogManager.getLogger(Main.class);

  @Autowired GeoDataLoader geoDataLoader;
  @Autowired RunInfoDao runInfoDao;
  @Autowired OtoDomCrawler crawler;

  public static void main(String[] args) {
    SpringApplication.run(Main.class, args);
  }

  @Override
  public void run(String... args) {
    Date startDate = new Date();

    if (args.length == 0 || args[0].toUpperCase().equals(RunMode.ALL.name())) {
      logger.info("Starting full mode.");

      /* Step 1 - download flat_app.flat data */

      Integer numberOfFlatsDownloaded = crawler.downloadFlats();
      RunInfo runInfo = new RunInfo(startDate, numberOfFlatsDownloaded);

      /*Step 2 - download coordinates*/

      geoDataLoader.fillFlatsWithCoordinates();
      runInfoDao.save(runInfo);

    } else {
      logger.info("Loading only coordinates.");
      geoDataLoader.fillFlatsWithCoordinates();
    }
  }
}
//runInfoDao.findAll().forEach(e -> System.out.println(e.toString()));